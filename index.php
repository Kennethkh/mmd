<?php

	$a = "Dette er en tekststreng<br>";
	echo $a;

	$name = "Kenneth";
	echo "Hej, $name <br>";
	echo "Hej, $name";

	$a = "<p>tekststreng af afsnitselementer</p>";
	echo $a;

	date_default_timezone_set('Europe/Copenhagen');
	$a = "<div> Dato i dag er: " . date("d. m Y") . " og kl er: " . date("h:i:s");
	echo $a;

	echo "<br>";

	echo "Er de to variabler ens? <br>";
	$a = "abc";
	$b = "abc";

	if ($a == $b) {
		echo "Ja, $a og $b er ens <br>";
	}

	$a = 1;

	if ($a == true) {
		echo "$a er true <br>";
	}

	$a = 0;

	if ($a == true) {
		echo "$a er true";
	} else {
		echo "$a er false";
	}